//
//  GameView.m
//  KeepUp
//
//  Created by Peter Devlin on 3/30/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "GameView.h"

@implementation GameView

@synthesize ball;
@synthesize tapLoc;
@synthesize scoreLabel;
float boundaryMargin = 10.0;
float actualHeight;
float actualWidth;
int score = 0;
int rotationFactor = 0;
UIImageView * imageView;
UIImage * img;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)startGame
{
    actualHeight = self.frame.size.height;
    actualWidth = self.frame.size.width;
    UIImageView * border = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border.png"]];
    [border setFrame:CGRectMake(0.0, 0.0, actualWidth, actualHeight)];
    ball = [[Ball alloc] initWithFrame:CGRectMake(actualWidth/2, -10, actualWidth/4, actualWidth/4)];
    img = [UIImage imageNamed:@"pokeball.png"];
    imageView = [[UIImageView alloc] initWithImage:img];
    [ball addSubview:imageView];
    [ball setDx:0];
    [ball setDy:0];
    [self addSubview:ball];
    [self addSubview:border];
    
}

-(void)restart
{
    score = 0;
    [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", score]];
    [ball setCenter:CGPointMake(actualWidth/2, -10)];
    [ball setDx:0];
    [ball setDy:0];
}


-(void)arrange:(CADisplayLink *)sender
{
    // Apply gravity to the acceleration of the ball
    [ball setDy:[ball dy] - .25];
    
    // Ball moves in the direction of gravity
    CGPoint p = [ball center];
    p.x += [ball dx];
    p.y -= [ball dy];
    if(p.y > actualHeight - 10)
    {
        [self restart];
        return;
    }
    
    //This image rotation code was from chuckstar on
    //http://www.chuckstar.com/blog/technology/iphone-sample-rotating-uiimage-inside-uiimageview/
    CGAffineTransform rotate = CGAffineTransformMakeRotation(rotationFactor * 3.14159 / 180.0);
    [imageView setTransform:rotate];
    
    // If we have gone too far left, or too far right, bounce off the side
    if (p.x < boundaryMargin || p.x > actualWidth - boundaryMargin) {
        [ball setDx:-ball.dx];
    }
    [ball setCenter:p];
    if(ball.dx > 0) rotationFactor++;
    if(ball.dx < 0) rotationFactor--;
}

-(IBAction)hit: (UITapGestureRecognizer*) s
{
    CGPoint collisionPoint = [s locationInView: self];
    if(CGRectContainsPoint(ball.frame, collisionPoint) && ball.center.y > actualHeight/3 && ball.dy < 0)
    {
        [ball setDy: 12];
        [ball setDx: 0.13*(ball.frame.origin.x - collisionPoint.x)];
        score++;
        [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", score]];
    }
}

@end
