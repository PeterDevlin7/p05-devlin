//
//  GameView.h
//  KeepUp
//
//  Created by Peter Devlin on 3/30/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ball.h"

@interface GameView : UIView

@property (nonatomic) CGPoint tapLoc;
@property (nonatomic, strong) Ball* ball;
@property (nonatomic, strong) IBOutlet UILabel *scoreLabel;

-(void)arrange:(CADisplayLink *)sender;
-(void)startGame;

@end