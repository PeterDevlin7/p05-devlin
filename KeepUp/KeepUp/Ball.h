//
//  Ball.h
//  KeepUp
//
//  Created by Peter Devlin on 3/30/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ball : UIView

@property (nonatomic) float dx, dy;  // Velocity
@property (nonatomic) CGRect rect;   // Frame

@end