//
//  ViewController.h
//  KeepUp
//
//  Created by Peter Devlin on 3/30/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet GameView *gameView;
@property (nonatomic, strong) CADisplayLink *displayLink;

@end